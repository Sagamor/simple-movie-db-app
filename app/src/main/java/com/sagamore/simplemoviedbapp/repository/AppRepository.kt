package com.sagamore.simplemoviedbapp.repository

import com.sagamore.simplemoviedbapp.data.model.CreditResponse
import com.sagamore.simplemoviedbapp.data.MovieService
import com.sagamore.simplemoviedbapp.data.model.GenreResponse
import com.sagamore.simplemoviedbapp.data.model.Popular
import io.reactivex.Single
import javax.inject.Inject

/**
 * @author a.v.davtyan
 */
interface AppRepository {

    fun getPopular(page: Int?): Single<Popular>

    fun getGenre(): Single<GenreResponse>

    fun getCredits(movieId: Int): Single<CreditResponse>
}

class AppRepositoryImpl @Inject constructor(private val apiService: MovieService): AppRepository {

    override fun getPopular(page: Int?): Single<Popular> {
        return apiService.getPopular(page=page)
    }

    override fun getGenre(): Single<GenreResponse> {
        return apiService.getGenres()
    }

    override fun getCredits(movieId: Int): Single<CreditResponse> {
        return apiService.getCredits(movieId)
    }
}
