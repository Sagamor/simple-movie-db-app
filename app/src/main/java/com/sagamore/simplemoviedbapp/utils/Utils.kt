package com.sagamore.simplemoviedbapp.utils

import android.content.Context
import android.os.Build
import androidx.annotation.RequiresApi
import java.time.LocalDate
import java.time.format.DateTimeFormatter

/**
 * @author a.v.davtyan
 */
fun calculateNumOfColumns(context: Context): Int {
    val displayMetrics = context.resources.displayMetrics
    val dpWidth = displayMetrics.widthPixels / displayMetrics.density
    val scalingFactor = 200
    val columnCount = (dpWidth / scalingFactor).toInt()
    return if (columnCount >= 2) columnCount else 2
}

@RequiresApi(Build.VERSION_CODES.O)
fun convertDate(from: String, pattern: String) : String {
    val formatter = DateTimeFormatter.ofPattern(pattern)
    val date = LocalDate.parse(from)
    return date.format(formatter)
}
