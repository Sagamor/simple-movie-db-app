package com.sagamore.simplemoviedbapp.ui.main

import com.sagamore.simplemoviedbapp.BaseViewModel
import com.sagamore.simplemoviedbapp.data.model.Popular
import com.sagamore.simplemoviedbapp.data.model.Result
import com.sagamore.simplemoviedbapp.repository.AppRepositoryImpl
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.observers.DisposableSingleObserver
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

/**
 * @author a.v.davtyan
 */
class PopularViewModel @Inject constructor(private val repository: AppRepositoryImpl) : BaseViewModel<Result>() {

    override fun fetchData(param: Int?) {
        loading.value = true
        disposable!!.add(
            repository.getPopular(param).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(object : DisposableSingleObserver<Popular>() {
                    override fun onError(e: Throwable) {
                        castsLoadError.value = true
                        loading.value = false
                    }

                    override fun onSuccess(value: Popular) {
                        castsLoadError.value = false
                        data.value = value.results
                        loading.value = false
                    }
                })
        )
    }
}
