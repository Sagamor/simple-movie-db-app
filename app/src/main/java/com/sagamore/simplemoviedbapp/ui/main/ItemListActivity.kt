package com.sagamore.simplemoviedbapp.ui.main

import android.os.Bundle
import androidx.activity.viewModels
import androidx.appcompat.widget.Toolbar
import androidx.core.widget.NestedScrollView
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.sagamore.simplemoviedbapp.R
import com.sagamore.simplemoviedbapp.utils.ViewModelFactory
import com.sagamore.simplemoviedbapp.utils.calculateNumOfColumns
import dagger.android.support.DaggerAppCompatActivity
import javax.inject.Inject

/**
 * @author a.v.davtyan
 */
class ItemListActivity : DaggerAppCompatActivity() {

    /**
     * Whether or not the activity is in two-pane mode, i.e. running on a tablet
     * device.
     */
    private var twoPane: Boolean = false

    private lateinit var recyclerView: RecyclerView

    @Inject
    lateinit var viewModelFactory: ViewModelFactory

    private val popularViewModel: PopularViewModel by viewModels(factoryProducer = { viewModelFactory })

    private val adapter = SimpleItemRecyclerViewAdapter(this, listOf(), twoPane)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_item_list)

        val toolbar = findViewById<Toolbar>(R.id.movieListToolbar)
        setSupportActionBar(toolbar)
        toolbar.title = title

        recyclerView = findViewById(R.id.item_list)
        recyclerView.layoutManager = GridLayoutManager(this, calculateNumOfColumns(this))
        recyclerView.adapter = adapter

        popularViewModel.fetchData(param = 1)

        popularViewModel.data.observe(this, {
            adapter.updateValues(it)
        })

        if (findViewById<NestedScrollView>(R.id.item_detail_container) != null) {
            twoPane = true
        }
    }
}
