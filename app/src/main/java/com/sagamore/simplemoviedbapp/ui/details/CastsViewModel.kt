package com.sagamore.simplemoviedbapp.ui.details

import com.sagamore.simplemoviedbapp.BaseViewModel
import com.sagamore.simplemoviedbapp.data.model.Cast
import com.sagamore.simplemoviedbapp.data.model.CreditResponse
import com.sagamore.simplemoviedbapp.repository.AppRepositoryImpl
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.observers.DisposableSingleObserver
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

/**
 * @author a.v.davtyan
 */
class CastsViewModel @Inject constructor(private val repository: AppRepositoryImpl) : BaseViewModel<Cast>() {

    override fun fetchData(param: Int?) {
        loading.value = true
        disposable!!.add(
            repository.getCredits(param!!).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(object : DisposableSingleObserver<CreditResponse>() {
                    override fun onError(e: Throwable) {
                        castsLoadError.value = true
                        loading.value = false
                    }

                    override fun onSuccess(value: CreditResponse) {
                        data.value = value.cast
                        castsLoadError.value = false
                        loading.value = false
                    }
                })
        )
    }
}
