package com.sagamore.simplemoviedbapp.ui.details

import com.sagamore.simplemoviedbapp.BaseViewModel
import com.sagamore.simplemoviedbapp.repository.AppRepositoryImpl
import com.sagamore.simplemoviedbapp.data.model.Genre
import com.sagamore.simplemoviedbapp.data.model.GenreResponse
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.observers.DisposableSingleObserver
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

/**
 * @author a.v.davtyan
 */
class GenreViewModel @Inject constructor(private val repository: AppRepositoryImpl) : BaseViewModel<Genre>() {

    override fun fetchData(param: Int?) {
        loading.value = true
        disposable!!.add(
            repository.getGenre().subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(object : DisposableSingleObserver<GenreResponse>() {
                    override fun onError(e: Throwable) {
                        castsLoadError.value = true
                        loading.value = false
                    }

                    override fun onSuccess(value: GenreResponse) {
                        castsLoadError.value = false
                        data.value = value.genres
                        loading.value = false
                    }
                })
        )
    }
}
