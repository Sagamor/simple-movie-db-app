package com.sagamore.simplemoviedbapp.ui.details

import android.annotation.SuppressLint
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.ProgressBar
import android.widget.TextView
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.sagamore.simplemoviedbapp.*
import com.sagamore.simplemoviedbapp.data.model.Cast
import com.sagamore.simplemoviedbapp.data.model.Genre
import com.sagamore.simplemoviedbapp.data.model.Result
import com.sagamore.simplemoviedbapp.utils.ViewModelFactory
import com.sagamore.simplemoviedbapp.utils.convertDate
import dagger.android.support.DaggerFragment
import net.opacapp.multilinecollapsingtoolbar.CollapsingToolbarLayout
import javax.inject.Inject

/**
 * @author a.v.davtyan
 */
class ItemDetailFragment : DaggerFragment() {

    @Inject
    lateinit var viewModelFactory: ViewModelFactory

    private var item: Result? = null

    private val castsViewModel: CastsViewModel by viewModels(factoryProducer = { viewModelFactory })

    private val genreViewModel: GenreViewModel by viewModels(factoryProducer = { viewModelFactory })

    private lateinit var recyclerView: RecyclerView
    private lateinit var progressBar: ProgressBar

    @RequiresApi(Build.VERSION_CODES.N)
    @SuppressLint("CheckResult")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        arguments?.let {
            if (it.containsKey(ARG_ITEM_ID)) {
                item = it.getSerializable(ARG_ITEM_ID) as Result?
            }
        }
    }

    @RequiresApi(Build.VERSION_CODES.O)
    @SuppressLint("SetTextI18n")
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val rootView = inflater.inflate(R.layout.fragment_item_detail, container, false)
        progressBar = rootView.findViewById(R.id.progressBar)
        recyclerView = rootView.findViewById(R.id.item_list)

        item?.let {
            rootView.findViewById<TextView>(R.id.overview).text = it.overview
            activity?.findViewById<CollapsingToolbarLayout>(R.id.toolbar_layout)?.title = it.title
            activity?.findViewById<ImageView>(R.id.preview)?.let { it1 ->
                Glide.with(this)
                    .load("https://image.tmdb.org/t/p/" + "w500" + it.posterPath)
                    .into(it1)
            }
            activity?.findViewById<ImageView>(R.id.backdrop)?.let { it2 ->
                Glide.with(this)
                    .load("https://image.tmdb.org/t/p/" + "w500" + it.backdropPath)
                    .into(it2)
            }
            activity?.findViewById<TextView>(R.id.releaseDate)?.text = "Released: ${
                convertDate(
                    it.releaseDate,
                    "dd MMM yyyy"
                )
            }"
            activity?.findViewById<TextView>(R.id.rating)?.text = "Rating: ${it.voteAverage * 10}"
        }

        item?.id?.let {
            castsViewModel.fetchData(it)
        }

        genreViewModel.fetchData()
        return rootView
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        castsViewModel.data.observe(viewLifecycleOwner, {
            if (it.isNotEmpty()) {
                updateCastRecyclerView(it)
            }
        })

        castsViewModel.getError().observe(viewLifecycleOwner, ViewModelErrorObserver())
        castsViewModel.getLoading().observe(viewLifecycleOwner, ViewModelLoadingObserver())

        genreViewModel.data.observe(viewLifecycleOwner, { it ->
            if (it.isNotEmpty()) {

                activity?.findViewById<TextView>(R.id.genre)?.apply {
                    text = toItemsString(it.filter { item?.genreIds?.contains(it.id)!! }
                        .toList())
                }
                Log.d("MovieDBApp", "There are ${it.size} Genres")
            }
        })
    }

    private fun updateCastRecyclerView(castList: List<Cast>) {
        recyclerView.adapter = CastAdapter(castList)
    }

    private fun toItemsString(genre: List<Genre>): String {

        return with(StringBuilder()) {
            genre.forEach {
                this.append("${it.name}, ")
            }
            this.toString()
                .substring(0, this.length - 2)
        }
    }

    private inner class ViewModelLoadingObserver : Observer<Boolean> {
        override fun onChanged(isLoading: Boolean?) {
            if (isLoading != null) {
                Log.d("MovieDBApp", "fetchData: !isLoading")
                if (isLoading) {
                    progressBar.visibility = View.VISIBLE
                    recyclerView.visibility = View.GONE
                    Log.d("MovieDBApp", "fetchData: isLoading")
                }
                else {
                    progressBar.visibility = View.GONE
                    recyclerView.visibility = View.VISIBLE
                }
            }
        }
    }

    private inner class ViewModelErrorObserver : Observer<Boolean> {

        override fun onChanged(isError: Boolean?) {
            if (isError != null) if (isError) {
                Log.d("MovieDBApp", "fetchData: isError != null")
                recyclerView.visibility = View.GONE
                Toast.makeText(activity, "There are some trouble in loading process", Toast.LENGTH_SHORT).show()
            } else {
                Log.d("MovieDBApp", "fetchData: isError = null")
                recyclerView.visibility = View.VISIBLE
            }
        }
    }

    companion object {
        const val ARG_ITEM_ID = "item"
    }
}
