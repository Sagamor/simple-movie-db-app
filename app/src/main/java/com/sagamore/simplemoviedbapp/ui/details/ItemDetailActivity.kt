package com.sagamore.simplemoviedbapp.ui.details

import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import com.sagamore.simplemoviedbapp.ui.main.ItemListActivity
import com.sagamore.simplemoviedbapp.R
import dagger.android.support.DaggerAppCompatActivity

/**
 * @author a.v.davtyan
 */
class ItemDetailActivity : DaggerAppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_item_detail)
        setSupportActionBar(findViewById(R.id.detail_toolbar))

        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        if (savedInstanceState == null) {

            ItemDetailFragment().apply {
                arguments = Bundle().apply {
                    putSerializable(
                        ItemDetailFragment.ARG_ITEM_ID,
                        intent.getSerializableExtra(ItemDetailFragment.ARG_ITEM_ID)
                    )
                }
            }.let {
                supportFragmentManager.beginTransaction()
                    .add(R.id.item_detail_container, it)
                    .commit()
            }
        }
    }

    override fun onOptionsItemSelected(item: MenuItem) =
        when (item.itemId) {
            android.R.id.home -> {
                navigateUpTo(Intent(this, ItemListActivity::class.java))
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
}
