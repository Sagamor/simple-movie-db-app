package com.sagamore.simplemoviedbapp.ui.main

import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.annotation.RequiresApi
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.sagamore.simplemoviedbapp.R
import com.sagamore.simplemoviedbapp.data.model.Result
import com.sagamore.simplemoviedbapp.ui.details.ItemDetailActivity
import com.sagamore.simplemoviedbapp.ui.details.ItemDetailFragment
import com.sagamore.simplemoviedbapp.utils.convertDate

/**
 * @author a.v.davtyan
 */
class SimpleItemRecyclerViewAdapter(
    private val parentActivity: ItemListActivity,
    private var values: List<Result>,
    private val twoPane: Boolean
) :
    RecyclerView.Adapter<SimpleItemRecyclerViewAdapter.ViewHolder>() {

    private val onClickListener: View.OnClickListener

    init {
        onClickListener = View.OnClickListener { v ->
            val item = v.tag as Result
            if (twoPane) {
                val fragment = ItemDetailFragment().apply {
                    arguments = Bundle().apply {
                        putSerializable(ItemDetailFragment.ARG_ITEM_ID, item)
                    }
                }
                parentActivity.supportFragmentManager
                    .beginTransaction()
                    .replace(R.id.item_detail_container, fragment)
                    .commit()
            } else {
                val intent = Intent(v.context, ItemDetailActivity::class.java).apply {
                    putExtra(ItemDetailFragment.ARG_ITEM_ID, item)
                }
                v.context.startActivity(intent)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.movie_card, parent, false)
        return ViewHolder(view)
    }

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = values[position]

        holder.apply {
            title.text = item.title
            releaseDate.text = convertDate(item.releaseDate, "dd MMM yyyy")
            Glide.with(preview)
                .load("https://image.tmdb.org/t/p/" + "w500" + item.posterPath)
                .into(preview)
        }

        with(holder.itemView) {
            tag = item
            setOnClickListener(onClickListener)
        }
    }

    override fun getItemCount() = values.size

    fun updateValues(values: List<Result>) {
        this.values = values
        notifyDataSetChanged()
    }

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val title: TextView = view.findViewById(R.id.title)
        val releaseDate: TextView = view.findViewById(R.id.releaseDate)
        val preview: ImageView = view.findViewById(R.id.icon)
    }
}
