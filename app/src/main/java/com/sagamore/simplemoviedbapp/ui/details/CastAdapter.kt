package com.sagamore.simplemoviedbapp.ui.details

import android.os.Build
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.annotation.RequiresApi
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.sagamore.simplemoviedbapp.data.model.Cast
import com.sagamore.simplemoviedbapp.R

/**
 * @author a.v.davtyan
 */
class CastAdapter(
    private val values: List<Cast>
) :
    RecyclerView.Adapter<CastAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.cast_card, parent, false)
        return ViewHolder(view)
    }

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = values[position]

        holder.apply {
            name.text = item.name
            role.text = item.character
            Glide.with(photo)
                .load("https://image.tmdb.org/t/p/" + "w500" + item.profilePath)
                .into(photo)
        }
    }

    override fun getItemCount() = values.size

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val name: TextView = view.findViewById(R.id.name)
        val role: TextView = view.findViewById(R.id.role)
        val photo: ImageView = view.findViewById(R.id.icon)
    }
}
