package com.sagamore.simplemoviedbapp.data.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

/**
 * @author a.v.davtyan
 */
data class GenreResponse(
    @SerializedName("genres")
    @Expose
    val genres: List<Genre>
)

data class Genre (
    @SerializedName("id")
    val id: Int,

    @SerializedName("name")
    val name: String
)
