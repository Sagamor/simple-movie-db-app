package com.sagamore.simplemoviedbapp.data

import com.sagamore.simplemoviedbapp.data.model.CreditResponse
import com.sagamore.simplemoviedbapp.data.model.GenreResponse
import com.sagamore.simplemoviedbapp.data.model.Popular
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

/**
 * @author a.v.davtyan
 */
interface MovieService {
    @GET("movie/popular?")
    fun getPopular(
        @Query("page") page: Int?
    ): Single<Popular>

    @GET("genre/movie/list")
    fun getGenres(
    ): Single<GenreResponse>

    @GET("movie/{movie_id}/credits")
    fun getCredits(
        @Path("movie_id") movieId: Int,
    ): Single<CreditResponse>
}
