package com.sagamore.simplemoviedbapp

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModel
import io.reactivex.disposables.CompositeDisposable

/**
 * @author a.v.davtyan
 */
abstract class BaseViewModel<T> : ViewModel(){

    protected var disposable: CompositeDisposable? = CompositeDisposable()
    val data: MutableLiveData<List<T>> = MutableLiveData<List<T>>()
    protected val castsLoadError = MutableLiveData<Boolean>()
    protected val loading = MutableLiveData<Boolean>()

    fun getError(): LiveData<Boolean> {
        return castsLoadError
    }

    fun getLoading(): LiveData<Boolean> {
        return loading
    }

    abstract fun fetchData(param: Int? = null)

    override fun onCleared() {
        super.onCleared()
        if (disposable != null) {
            disposable!!.clear()
            disposable = null
        }
    }
}
