package com.sagamore.simplemoviedbapp.di.module

import com.sagamore.simplemoviedbapp.ui.details.ItemDetailFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

/**
 * @author a.v.davtyan
 */
@Module
interface DetailFragmentBindingModule {

    @ContributesAndroidInjector
    fun provideDetailFragment(): ItemDetailFragment?
}
