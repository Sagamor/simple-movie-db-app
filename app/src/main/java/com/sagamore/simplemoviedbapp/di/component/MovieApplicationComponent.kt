package com.sagamore.simplemoviedbapp.di.component

import android.app.Application
import com.sagamore.simplemoviedbapp.app.BaseApplication
import com.sagamore.simplemoviedbapp.di.module.ActivityBindingModule
import com.sagamore.simplemoviedbapp.di.module.ApplicationModule
import com.sagamore.simplemoviedbapp.di.module.ContextModule
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjector
import dagger.android.support.AndroidSupportInjectionModule
import dagger.android.support.DaggerApplication
import javax.inject.Singleton

/**
 * @author a.v.davtyan
 */

@Singleton
@Component(modules = [ContextModule::class, ApplicationModule::class, AndroidSupportInjectionModule::class, ActivityBindingModule::class])
interface MovieApplicationComponent : AndroidInjector<DaggerApplication?> {

    fun inject(application: BaseApplication?)

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(application: Application?): Builder?
        fun build(): MovieApplicationComponent?
    }
}
