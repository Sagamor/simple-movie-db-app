package com.sagamore.simplemoviedbapp.di.module

import android.util.Log
import com.google.gson.GsonBuilder
import com.sagamore.simplemoviedbapp.data.MovieService
import dagger.Module
import dagger.Provides
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.Response
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton


/**
 * @author a.v.davtyan
 */
@Module(includes = [ViewModelModule::class])
class ApplicationModule {

    companion object {
        private const val BASE_URL = "https://api.themoviedb.org/3/"
    }

    @Singleton
    @Provides
    fun provideRetrofit(): Retrofit {
        val logging =
            HttpLoggingInterceptor()
                .setLevel(HttpLoggingInterceptor.Level.BODY)

        val gson = GsonBuilder()
            .setLenient()
            .create()

        return Retrofit.Builder()
            .baseUrl(BASE_URL)
            .client(
                OkHttpClient().newBuilder()
                    .addInterceptor(TokenInterceptor())
                    .addInterceptor(logging)
                    .build()
            )
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .addConverterFactory(GsonConverterFactory.create(gson))
            .build()
    }

    class TokenInterceptor : Interceptor {

        companion object {
            private const val API_KEY = "api_key"
            private const val API_KEY_VALUE = "274f828ad283bd634ef4fc1ee4af255f"
        }

        override fun intercept(chain: Interceptor.Chain): Response {
            var original = chain.request()
            val url = original.url.newBuilder()
                .addQueryParameter(API_KEY, API_KEY_VALUE).build()
            original = original.newBuilder().url(url).build()
            Log.e("MovieDBApp", "intercept: ${original.url}")
            return chain.proceed(original)
        }
    }

    @Singleton
    @Provides
    fun provideRetrofitService(retrofit: Retrofit): MovieService {
        return retrofit.create(MovieService::class.java)
    }
}
