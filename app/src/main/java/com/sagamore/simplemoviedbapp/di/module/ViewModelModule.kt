package com.sagamore.simplemoviedbapp.di.module

import androidx.lifecycle.ViewModel
import com.sagamore.simplemoviedbapp.ui.details.CastsViewModel
import com.sagamore.simplemoviedbapp.di.qualifiers.ViewModelKey
import com.sagamore.simplemoviedbapp.ui.details.GenreViewModel
import com.sagamore.simplemoviedbapp.ui.main.PopularViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

/**
 * @author a.v.davtyan
 */
@Module
interface ViewModelModule {

    @Binds
    @IntoMap
    @ViewModelKey(CastsViewModel::class)
    fun bindCastsViewModel(viewModel: CastsViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(GenreViewModel::class)
    fun bindGenreViewModel(viewModel: GenreViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(PopularViewModel::class)
    fun bindPopularViewModel(viewModel: PopularViewModel): ViewModel
}
