package com.sagamore.simplemoviedbapp.di.module

import android.app.Application
import android.content.Context
import dagger.Binds
import dagger.Module

/**
 * @author a.v.davtyan
 */
@Module
interface ContextModule {
    @Binds
    fun provideContext(application: Application?): Context?
}
