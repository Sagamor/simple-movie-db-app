package com.sagamore.simplemoviedbapp.di.module

import com.sagamore.simplemoviedbapp.ui.details.ItemDetailActivity
import com.sagamore.simplemoviedbapp.ui.main.ItemListActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

/**
 * @author a.v.davtyan
 */
@Module
interface ActivityBindingModule {

    @ContributesAndroidInjector(modules = [DetailFragmentBindingModule::class])
    fun bindItemDetailActivity(): ItemDetailActivity?

    @ContributesAndroidInjector(modules = [DetailFragmentBindingModule::class])
    fun bindItemListActivity(): ItemListActivity?
}
