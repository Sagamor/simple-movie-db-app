package com.sagamore.simplemoviedbapp.app

import android.util.Log
import com.sagamore.simplemoviedbapp.di.component.DaggerMovieApplicationComponent
import com.sagamore.simplemoviedbapp.di.component.MovieApplicationComponent
import dagger.android.AndroidInjector
import dagger.android.support.DaggerApplication


/**
 * @author a.v.davtyan
 */
class BaseApplication: DaggerApplication() {
    override fun onCreate() {
        super.onCreate()
        Log.d("MovieDBApp", "BaseApplication onCreate ")
    }

    override fun applicationInjector(): AndroidInjector<DaggerApplication?> {
        val component: MovieApplicationComponent? =
            DaggerMovieApplicationComponent.builder().application(this)?.build()
        component?.inject(this)
        return component!!
    }
}
